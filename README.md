# DNS Report

Written elaboration about the DNS integration and filtering of the research project [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns).

## Evaluation of DNS-filtering with real-time ML predictions

The fully build PDF file can be found [here](https://mldns.gitlab.io/dns-report/dns-report.pdf).

This report focuses mainly on the DNS integration and filtering aspect of the research project [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns).

To see more detailed steps or to try it for yourself, the repository [mldns-meta](https://gitlab.com/mldns/mldns-meta) is well suited.
*mldns-meta* combines the two repositories [Unbound](https://gitlab.com/mldns/unbound) and [WebService](https://gitlab.com/mldns/webservice) and simplifies the start by providing small scripts.
