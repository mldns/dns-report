%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[fleqn,10pt]{SelfArx} % Document font size and equations flushed left

\usepackage[english]{babel} % Specify a different language here - english by default

\usepackage{lipsum} % Required to insert dummy text. To be removed otherwise

\usepackage{pdfpages}

\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}

\usepackage{acronym}

\usepackage[newfloat]{minted}

\usepackage{float}

\usepackage{tabularx}

\usepackage{listings}

\usepackage{soul}

\usepackage{xcolor}

\newenvironment{code}{\captionsetup{type=listing}}{}
\SetupFloatingEnvironment{listing}{name=Source Code}

%----------------------------------------------------------------------------------------
%	COLUMNS
%----------------------------------------------------------------------------------------

\setlength{\columnsep}{0.55cm} % Distance between the two columns of text
\setlength{\fboxrule}{0.75pt} % Width of the border around the abstract

%----------------------------------------------------------------------------------------
%	COLORS
%----------------------------------------------------------------------------------------

\definecolor{color1}{RGB}{0,0,0} % Color of the article title and sections
\definecolor{color2}{RGB}{0,20,20} % Color of the boxes behind the abstract and headings

%----------------------------------------------------------------------------------------
%	HYPERLINKS
%----------------------------------------------------------------------------------------

\hypersetup{
	hidelinks,
	colorlinks,
	breaklinks=true,
	urlcolor=color2,
	citecolor=color1,
	linkcolor=color1,
	bookmarksopen=false,
	pdftitle={Title},
	pdfauthor={Author},
}

%----------------------------------------------------------------------------------------
%	ARTICLE INFORMATION
%----------------------------------------------------------------------------------------

\JournalInfo{Research Project SoSe 2021, University of Applied Sciences Cologne} % Journal information
\Archive{\today} % Additional notes (e.g. copyright, DOI, review/research article)

% \PaperTitle{Evaluation of ML-guided DNS-based threat-prevention in home networks} % Article title
\PaperTitle{Evaluation of DNS-filtering with real-time ML predictions}

\Authors{Léon Lenzen (\textit{leon\_gerrit.lenzen@smail.th-koeln.de})} % Authors

% \affiliation{\textsuperscript{1}\textit{leon\_gerrit.lenzen@smail.th-koeln.de, University of Applied Sciences Cologne}} % Author affiliation


\Keywords{Machine Learning --- DNS --- Filtering --- Security --- Home Network} % Keywords - if you don't want any simply remove all the text between the curly brackets
\newcommand{\keywordname}{Keywords} % Defines the keywords heading name

\newcommand*{\quelle}{%
  \footnotesize Source:
}

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\Abstract{
    The landscape of security solutions for home networks focuses mostly on per device security measures like software firewall or anti virus software. But in a household where the number of devices is constantly increasing, this approach is no longer practical. Therefore, this paper introduces \acs{mldns}, a software system which will act as \acs{dns} server for the home network and will protect the users from accessing malicious domains. The static allow and block lists currently in use by other \acs{dns} filtering software must be maintained and updated on a regular basis. That is why \ac{mldns} has a \acl{ml} component that predicts a maliciousness score for each unknown domain. 

    The goal was to build a small and efficient system, which could be integrated into a home router. A prototype on a 
    \acl{rpi4} was built and it performed very well. The time for the prediction was around 1 \textit{ms} and the overall introduced latency compared to a normal \acs{dns} resolution was around 2.7 \textit{ms}. The accuracy of the best-fit model was 95.8\%.
}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Output the title and abstract box

\tableofcontents % Output the contents section

\thispagestyle{empty} % Removes page numbering from the first page

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------
%------------------------------------------------
\section{Introduction}

Today, few measures are in place in home networks to protect users. In Germany most \acp{iad} provide simple firewall capabilities, reducing the risk of intrusion via incoming requests from the Internet \cite{avm:feat,dt:man}. But most home user targeted solutions do not focus on threats from inside the network. One way to also protect the network from the inside is to monitor the \ac{dns} traffic and block malicious requests. \ac{dns} is used to resolve domain names into \ac{ip} addresses or other types of \acp{rr}. It can be used either unencrypted or encrypted in the form of \ac{dot} or \ac{doh} \cite{rfc1035,rfc8484,rfc7858}. The blockage of malicious request can mitigate phishing attacks or communication with \ac{cc} servers for botnets. Static block list do not provide enough security in a fast changing landscape of the Internet \cite{10.1145/3419394.3423657}.
%------------------------------------------------
\section{Background}
Traditionally, \acp{iad} have a simple integrated firewall which blocks incoming requests from the Internet. Some even have the capability to block communication to known malicious \ac{ip} addresses, but \ac{ip} address owners change frequently and therefore the providers of such \ac{ip} block lists must always keep up with the current situation \cite{9416274,10.1145/3419394.3423657}. Many \acp{iad} also have a build in \ac{dns} forwarder which provides a local \ac{dns} server to the devices in the home network. Even thought the \ac{dns} protocol was initially designed in the late 80s, it is still one of the backbones of the modern internet. The first use-cases were the translation from a domain name to the \ac{ip} address and also the lookup of mailboxes. It was created with extensibility in mind, therefore many new \ac{rr} types were added to the protocol. \ac{dns} works like a phone book and can translate a name to a address. \cite{rfc1034}

Around 2016 started the discussion about encrypting \ac{dns} traffic with \ac{tls} to increase privacy and security \cite{rfc7858}. Two years later \ac{doh} was specified \cite{rfc8484}. \ac{doh} is preferred by modern browsers because they already have a working \ac{https} stack. \textit{Firefox} has started to enable \ac{doh} per default in some countries and partnered with two public \ac{doh} resolvers. This prevents \acp{isp} or public hotspot providers from tracking users' \ac{dns} queries, but at the same time all data is passed to a single commercial company and local \ac{dns} resolvers are bypassed \cite{TPRC47}.

These local \ac{dns} resolvers are normally distributed via \ac{dhcp} and sometimes also provide additional functionality like parental control or malware protection.

Many services on the Internet take into account the so-called \ac{IP} reputation. The \ac{ip} reputation is a measure for how reputable a \ac{ip} address or \ac{ip} address range is. This affects peering decisions or mail delivery from different \ac{ip} address ranges. Some \acp{isp} go quite some way to increase their \ac{ip} reputation by contacting each user on their network who was identified to be infected by some kind of malware \cite{telekom:flubot}.
\cite{USMAN2021124,9049760}

These malware softwares need to talk with their \ac{cc} servers to receive commands what to do or what not to do. There are multiple ways for a malware to communicate with a \ac{cc} server. One way would be simple \ac{https} or another way could be via \ac{dns} \acp{rr}. The communication via \ac{dns} is a good communication channel from the attackers point of view because it can disguise itself in the day to day traffic. On way to communicate via \ac{dns} could be with \ac{dga}-based domains. These are domains generated by a algorithm on the client (malware) side as well as by the \ac{cc}. Only a small portion of the \ac{dga}-based domains are actually registered but the malware itself tries to connect to hundreds of the thousand domains generated each day. This circumstance makes it hard for countermeasures to be effective. With the increasing popularity of deep neural networks in the recent years, also the detection of \ac{dga}-based domains become much more effective with a F1 score of up to 0.9906 \cite{woodbridge2016predicting}. It is estimated that nearly a third of the world wide threats can be prevented by using a \ac{dns} blocking mechanism \cite{gca:dns}. \cite{6461889}



%------------------------------------------------
\section{Approach}
In order to block any malicious behavior at the source, the home network, a local \ac{dns} resolver with blocking capabilities will be implemented in the home network. To not rely on static block-lists a new approach for home networks will be taken. For every domain in a \ac{dns} request the malicious intent of such will be predicted by a \ac{ml} algorithm. The flow for a simple \ac{dns} request in a home network is depict in \autoref{fig:dns_flow}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.48\textwidth]{figures/dns-flow.pdf}
  \caption{Flow of \ac{dns} request}
  \label{fig:dns_flow}
\end{figure}


%------------------------------------------------
\subsection{\ac{ml} Framework}
This prediction will be in real-time and therefore provide even good security for unknown threads. Training for the \ac{ml} algorithm will be done with a combination of lists with malicious and harmless domains. The evaluation of different \ac{ml} algorithms will be discussed in a further report \cite{mldns:florian}. \textit{FastText} is the \ac{ml} framework of choice, since it features several key features\cite{bojanowski2017enriching,fasttext}. By exposing a C++ and Python \ac{api}, integration into the system is easy. Furthermore, training of a new model and predictions for a domain are really fast compared to different \ac{ml} frameworks like \textit{TensorFlow}. But one of most significant advantages is the high accuracy while prediction the malicious intent of a domain \cite{t-challenge}. 

\textit{FastText} does use so-called n-grams, which are n character long subwords of the complete word, e.g. the word \textit{machine} breaks down to a bag of seven 3-grams: \textit{$<$ma}, \textit{mac}, \textit{ach}, \textit{chi}, \textit{hin}, \textit{ine} and \textit{ne$>$}. \textit{$<$} and \textit{$>$} are added to better differentiate short words from n-grams and also preserve the relevance of prefixes and suffixes. By using n-grams, \textit{FastText} does perform better with unknown words than similar approaches which use whole words. These n-grams are considered as input for the \ac{ml} algorithm and are passed through a hidden layer which is in fact the trained weight matrix. In the end the sum of all weighted n-grams is passed through a SoftMax activation function. \cite{bojanowski2017enriching,joulin2017bag}

%------------------------------------------------

\subsection{\ac{dns} Resolver}
The implementation of a full fledged \ac{dns} resolver is out of the scope for this research project. Therefore, a integration of the \ac{ml} algorithm into an existing \ac{dns} resolver is desired. One of the requirements for the \ac{dns} resolver is to expose an \ac{api} for dynamic blocking of \ac{dns} requests. The two most public \ac{dns} filtering solutions for home networks \textit{Adguard Home} and \textit{Pi-Hole} do not expose such \acp{api} \cite{dns:kalytta}. \textit{Unbound} does have a \ac{api} to dynamically interact with the incoming \ac{dns} requests \cite{unbound:pyunbound}. Because of the before mentioned adoption of \ac{doh} and \ac{dot} by newer clients, the local \ac{dns} resolver needs to provide support for these two protocols. Otherwise the clients will circumvent the local \ac{dns} resolver and try to connect to public remote \ac{doh}/\ac{dot} resolvers. \textit{Unbound} also supports connections from \ac{doh} and \ac{dot} clients \cite{unbound:doh}. Furthermore, it supports using \ac{dot} to encrypt request to the upstream \ac{dns} resolvers \cite{unbound:doh}.

%------------------------------------------------

\subsection{List Handling}
In addition to making a prediction for every new domain, the system will also provide a mechanism to overrule the prediction. This functionality is comparable to static allow- and block-listing. The user-defined lists are static and are only managed by the user himself. The same structure acts as a cache for the predictions from the \ac{ml} algorithm. By adding a configurable \ac{ttl} to each cache entry, the prediction does not have to be made with every request, but at the same time the domain will be reevaluated after the \ac{ttl} expired. Additionally, the \ac{ttl} provides a way to enforce privacy mechanism by dropping every domain which is older than e.g. one day.

%------------------------------------------------

\subsection{User Interaction}

The user interaction with the \ac{mldns} system should be minimal. Therefore, a approach was used to only interact with the user if he is about to access a malicious domain. When the user uses a browser he will be redirected to a website, which explains why the website he originally wanted to access was blocked. The user will also be given a choice to add the blocked domain to the allow list. 

Furthermore, a simple website to configure \ac{mldns} will be available to the user. The available settings include turning on and off the prediction with \textit{fastText} but also the consideration of the allow and block lists. In addition, the sensitivity of the prediction will be configurable. Another bit part of the website will be the administration of the allow and block lists.

All the different websites are designed to be usable by smartphones as well as desktop browsers.


%------------------------------------------------

\subsection{Deployment}
The two different software components will run in \textit{Docker} containers and will be tied together by \textit{Docker Compose} for easier usage. The following listing shows all ports exposed by the \textit{Docker} containers.
\begin{itemize}
  \item \underline{53}: plain-text \ac{dns} (\ac{dns} resolver)
  \item \underline{853}: \ac{dot} (\ac{dns} resolver)
  \item \underline{443}: \ac{doh} (\ac{dns} resolver)
  \item \underline{80}: \acs{http} block/\ac{api} server (list handling)
  \item \underline{8443}: \ac{https} block/\ac{api} server (list handling)
\end{itemize}

The communication between the two software components will be implemented in three different ways, specified in \autoref{sub-sec:rest-api} and later analysed in \autoref{sub-sec:container-latency} which will yield the best performance in regards to low latency.

%================================================
\section{Implementation}
%================================================

\subsection{Unbound Plugin}

The \textit{unbound plugin} is responsible for asking the \textit{list service} about the state of a domain and in an unknown state performing the prediction with \textit{fastText}. It also decides what response will be sent for a blocked domain.
\textit{Unbound} exposes a simple \textit{Python} interface to interact programmatically with the \ac{dns} query and format an answer. To use this interface, \textit{unbound} has to be compiled with the option \textit{--with-pythonmodule}. After that, the methods depict in \autoref{code:unbound-pythonmod} need to be implemented.

\begin{code}
  \captionof{listing}{Interface of unbound pythonmod}
  \begin{minted}[fontsize=\small]{python}
def init_standard(id, env):
def deinit(id):
def inform_super(id, qstate, superqstate, qdata):
def operate(id, event, qstate, qdata):
\end{minted}
  \label{code:unbound-pythonmod}
\end{code}

The \textit{init\_standard} method is used to load the \textit{fastText} model into memory and read the configuration from the \textit{list service}. In addition, the \textit{operate} method gets called by \textit{unbound} with every new incoming \ac{dns} request. To reduce unnecessary predictions and requests to the \textit{list service} a check for the \ac{dns} query type is in place. This will exclude some query types (e.g. for \ac{dnssec}) from being predicted and potentially blocked.

The \ac{dns} answer depends on the \ac{rr} type. The implementation supports currently two different answers. The first one is in place for A and AAAA \acp{rr} and will return the \ac{ipv4} respectively \ac{ipv6} address of the \textit{list service}. Furthermore, the reason will be discussed in \autoref{sub-sec:web-gui}.
\sethlcolor{lightgray}
A prediction is made by invoking the \textit{fastText} interface which holds the trained model \cite{t-challenge}. Before a domain is passed to \textit{fastText}, it gets preprocessed. This includes replacing \hl{ . }, \hl{ - }and \hl{ \_ } with spaces and also removing the trailing \hl{ . }, which represents the root domain zone.
Deciding between malicious and harmless is done by comparing the value with a threshold. New predictions are transferred to the \textit{list service} for caching.
\sethlcolor{yellow}


%------------------------------------------------
\subsection{List Service}
\label{sec:list-service}

The implementation of the \textit{list service} handles both the management of the block and allow list as well as displaying the web \ac{gui} for a blocked domain and to configure \textit{\ac{mldns}}. \textit{Golang}, an Open Source programming language developed by \textit{Google} was chosen for this task because it is fast and provides easy to use networking components \cite{go}.

\subsubsection{\ac{rest} \ac{api}}
\label{sub-sec:rest-api}

The \ac{api} design follows the \ac{rest} principle \cite{10.1145/514183.514185} and is documented with \textit{OpenAPI} \cite{oai:spec}. By combining two ways of list handling, a good compromise between speed and persistence can be achieved. For fast access during look-ups a key-value-based list is hold in memory. In order to provide a persistent option every entry in the in-memory list is mirrored to a corresponding \ac{csv} file. In total there are three different kinds of lists, also called levels:
\begin{itemize}
  \item personal: user-defined lists, can override every other list
  \item provider: bundled with the application to provide baseline, overrules \ac{ml} prediction, can be a community curated list
  \item \ac{mldns}: caching for \ac{ml} prediction
\end{itemize}
All \ac{api} methods work with all levels by either query-parameters or body-arguments.
For the in-memory lists a library called \textit{TTLCache} is used \cite{ttlcache}. It provides all the necessary functionality but still has a small footprint. Apart from the domain itself, the \ac{csv} entry contains the status (blocked or allowed) and the time of creation for the \ac{ttl}.

The \ac{api} endpoint to check the status of a domain is implemented in three different ways. The first implementation is a basic \ac{http} GET method like the rest of the \ac{api}. In addition a \textit{gRPC} endpoint is created where due to the usage of \textit{gRPC} the application data is transmitted as binary data and it takes advantages of \ac{http}/2 \cite{grpc}. For this, a contract needs to be designed first. This contract is defined in the \ac{protobuf} language and is used to generate the client code as well as some parts of the server code \cite{protobuf}. The third implementation uses \textit{WebSocket} communication where the data is streamed over an ongoing \ac{tcp} session \cite{rfc6455}.


\subsubsection{Web \ac{gui}}
\label{sub-sec:web-gui}

The web \ac{gui} is lightweight yet functional. \textit{Golang} provides a template engine for \ac{html} files which is used to serve server-rendered \ac{html} files to the client. For the block page the handler extracts the information about the domain entered into the \ac{url} and includes it in the explanation text for the blockage. The domain is either retrieved from the \textit{Host} header field in \ac{http}/1 \cite{rfc7230} or the \textit{:authority} pseudo-header field in \ac{http}/2 \cite{rfc7540}.

In addition, the user can change settings for the \textit{unbound plugin} which are requested from the \textit{unbound plugin} at every start. Interactivity on the page, e.g. editing the personal list, is done by integration of \textit{JavaScript}.

Apart from configuration, the web \ac{gui} also displays statistics about timing and frequency of prediction as well as rankings for the most requested malicious and harmless domains.

The detailed sequence of calls to the different implemented services can be found in \autoref{fig:seq_unknown}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.48\textwidth]{figures/unknown-domain.pdf}
  \caption{Sequence diagram for unknown domain}
  \label{fig:seq_unknown}
\end{figure}

%================================================
\section{Results}
%================================================

%------------------------------------------------
\subsection{\ac{ml} Accuracy}
Heinrich et al. \cite{mldns:florian} performed several experiments for training different \textit{fastText} models. 
The top 5 resulting models all yield a accuracy score over 0.95, where the best model even achieved 0.96023. All important metrics are displayed in \autoref{tab:top_10_ft_models}, which is sorted by accuracy followed by bad recall, top recall and model size.

Although three models exist that have better accuracy, a recommendation is made for the fourth model. This recommendation stems from the fact that the deviations in accuracy and other metrics are negligible, but at the same time the model size is significantly smaller.

\begin{table*}[t]
  \centering
  \begin{tabular}{|l|l|l|l|l|l|l|}
  \hline
  \textbf{RunID} & \textbf{Accuracy} & \textbf{Bad Precision} & \textbf{Bad Recall} & \textbf{Top Precision} & \textbf{Top Recall} & \textbf{Model Size in MB} \\ \hline
      7f87b0b & 0.96023 & 0.95260 & 0.96865 & 0.96811 & 0.95180 & 386.816 \\ \hline
      4df73c1 & 0.95968 & 0.95224 & 0.96790 & 0.96736 & 0.95145 & 194.313 \\ \hline
      048bca0 & 0.95933 & 0.95140 & 0.96810 & 0.96753 & 0.95055 & 91.078 \\ \hline
      d90b78f & 0.95803 & 0.95092 & 0.96590 & 0.96535 & 0.95015 & 61.488 \\ \hline
      ab2fc23 & 0.95800 & 0.94648 & 0.97090 & 0.97013 & 0.94510 & 194.306 \\ \hline
      % 9e42cff & 0.95788 & 0.94756 & 0.96940 & 0.96868 & 0.94635 & 212.496 \\ \hline
      % 538a72a & 0.95638 & 0.94877 & 0.96485 & 0.96424 & 0.94790 & 386.852 \\ \hline
      % 82f3010 & 0.95538 & 0.94642 & 0.96540 & 0.96469 & 0.94535 & 194.331 \\ \hline
      % 403d17b & 0.95525 & 0.94742 & 0.96400 & 0.96336 & 0.94650 & 47.979 \\ \hline
      % 4fb4d31 & 0.95458 & 0.94744 & 0.96255 & 0.96194 & 0.94660 & 107.180 \\ \hline
  \end{tabular}
  \caption{Top 5 \textit{fastText} models}
  \label{tab:top_10_ft_models}
\end{table*}




%------------------------------------------------
\subsection{\ac{ml} Prediction Performance}
A test setup including a \ac{rpi4} with 4GB of memory single board computer was created to gather sufficient data about the time a prediction needs \cite{rpi4b}. The hardware performance of a \ac{rpi4} is comparable to modern \acp{iad}. The software components were installed on the \ac{rpi4} and its \ac{ip} address was configured as default \ac{dns} server in the \ac{iad}. The default \ac{dns} server was distributed to the home network via \ac{dhcp}.
Over the course of two weeks all \ac{dns} requests and predictions were logged. In total, 300852 predictions were performed. From this number 299842 (99.66\%) domains were predicted harmless and only 1010 (0.34\%) domains were predicted malicious. The distribution of the prediction time for all the predictions can be seen in \autoref{fig:time_per_prediction}. Due to the large difference in quantity between malicious and harmless predictions the malicious times have a higher variance. Prediction times were constantly around one millisecond. The average prediction time for harmless domains was 1.109 \textit{ms} and 1.064 \textit{ms} for malicious domains.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.48\textwidth]{figures/time_per_prediction.png}
  \caption{Time per prediction}
  \label{fig:time_per_prediction}
\end{figure}

Loading the \textit{fastText} model into memory is also part of the performance. On one hand it takes between 1 and 10 seconds to load the model at every start of \textit{unbound}. This time depends of the size of the model. On the other hand it affects the \ac{ram} usage while using \ac{mldns}. The \ac{ram} usage is proportional to the model size. This means for a 125 \textit{MB} big model the \ac{ram} usage of unbound increases about 125 \textit{MB}, too. This consideration becomes relevant in the context of deploying \ac{mldns} at \acp{iad} because they have very limited hardware resources. By using a compression algorithm build into \textit{fastText}, the model size could be reduced to around 1 \textit{MB}. But it should be noted that choosing too strong compression parameters decreases the quality of the model. For the evaluation in this paper compression was not used.

%------------------------------------------------


\subsection{Inter-Container Latency}
\label{sub-sec:container-latency}

Besides the speed of the prediction, also the latency between the two containers \textit{unbound plugin} and \textit{list service} is very important. To evaluate the best method for inter-service communication, three different approaches were implemented and tested.

From a development effort point of view the simple \ac{http} request has the lowest effort but also introduced the highest latency with an average of 15 \textit{ms} until the response was received by the \textit{unbound plugin}. The two other methods \textit{gRPC} and \textit{WebSockets} reduced the latency to 2.7 \textit{ms} respectively 1.6 \textit{ms}.

The actual lookup of the domain in the lists took only 0.08 \textit{ms} in average which shows that most of the latency is introduced by the actual transport. Therefore, either \textit{gRPC} or \textit{WebSockets} should be the preferred transport mechanism. One advantage of \textit{gRPC} compared to \textit{WebSockets} is its robust type-system and code generation.
%------------------------------------------------

\subsection{Performance Impact}
The overall performance impact by introducing a real-time \ac{ml} prediction and adding a interactive list handling is around 2.7 \textit{ms}. Depending on the networking technology the relative impact varies greatly. When connecting to the \ac{dns} resolver via Ethernet the prediction and list-lookup are responsible for up to 25\% of the complete answering time. In a WiFi network the percentage drops to around 5\%.
%------------------------------------------------

\section{Related Works}
The company \textit{DNSFilter} provides a centralised \ac{dns} filter solution which uses \textit{Webshrinker}. Compared to the local \ac{dns} resolver developed in this paper, \textit{DNSFilter} provides a centralized \ac{dns} resolver which also provides thread-detection. \textit{Webshrinker} is the \ac{ml} application which detects threats by actually browsing the requested \ac{url} and evaluating over 20 threat markers. Apart from analyzing the structure and content of the website, \textit{Webshrinker} also performs image analysis to improve finding phishing sites. When comparing the prediction time for unknown domains, \ac{mldns} outperforms \textit{Webshrinker} by a significant margin. \ac{mldns} takes around 1 \textit{ms}, while \textit{Webshrinker} needs about 10 s due to the more involving analysis. For already known domains \textit{Webshrinker's} response time improves to around 200 \textit{ms}. It should be noted that for \textit{Webshrinker}, the response times are for the request to an \ac{api} on the Internet. \cite{dnsfilter:webshrinker:faq,dnsfilter:webshrinker}

In 2019 Spaulding \cite{jeffrey} proposed \textit{D-FENS}, a local \ac{dns} filtering resolver, follows the same idea of using real-time predictions to determine if a domain is malicious or benign. It also has the same philosophy of running in a home network. \textit{D-FENS} uses a deep neural network to predict a malicious score for a domain name. On the journey to the final \textit{D-FENS} system, the dissertation also discussed their approach to identifying \ac{dga}-based domains which they called \textit{DRIFT}. It uses a \ac{ncn} classifier as its \ac{ml} algorithm. Furthermore, \textit{D-FENS} implemented a \ac{dns} server from scratch in \textit{Python} with the help of libraries, which introduces additional development overhead for supporting \ac{doh} and \ac{dot}. \textit{D-FENS} does not support \ac{doh} and \ac{dot}, neither for client connections nor upstream lookups.
With equal consumer hardware (MacBook Pro 2.3 GHz Intel Core i5 with 8GB) the prediction of \ac{mldns} (0.5 \textit{ms}) is significant faster than the prediction of \textit{D-FENS} (2.5 - 3.1 \textit{ms}).

There are many more projects which combine \ac{ml} with \ac{dns} but most aim for threat detection with a retrospectively analysis of the \ac{dns} traffic.
\cite{fi10050043}

%================================================
\section{Conclusion}
%================================================
In this paper the development of \ac{mldns}, a system for reducing the risk of successful malware attacks in home networks, was shown. The developed \textit{unbound plugin} ties together the whole system composed of \textit{fastText} for predictions and \textit{list service} for list management and . 

It has been shown that the predictions by \textit{fastText} are fast enough to be executed in real-time. In addition, a real world test yielded positive feedback in the form that the activation of \ac{mldns} has not degraded the user experience in a home network. Even if a domain was blocked by accident, the user was given the choice to override the prediction.

Despite having a working prototype, there is still room for improvement. Currently the \textit{fastText} model can only differentiate between malicious and benign domains. A next evolution would be to include multiple categories which could introduce parental control capabilities to \ac{mldns}.

Currently the feedback about blocked domains is only directly available to a user if he wanted to access a malicious domain in a web browser. In a future version of \ac{mldns} a companion application is conceivable. With every new malicious domain a push notification would be sent to the application. Not only would this increase the compatibility with \ac{iot} devices but also increase the security because the allowlisting would be performed on a separate device or at least in a separate application. This would be comparable to two-factor authentication. 

To further improve the performance of the whole \ac{mldns} system, a new approach for communication between \textit{unbound plugin} and \textit{list service} could be proposed. This would be good because the measurements have shown, that the majority of the delay was introduced by communication between the two services. One example would be integrating the list management into the \textit{unbound plugin}.

Another possible enhancement would be online \ac{ml}, which would increase the capabilities of \ac{mldns} to adapt to its users. This was also proposed by Heinrich et al. \cite{mldns:florian}.


%----------------------------------------------------------------------------------------
\clearpage
\phantomsection
\section*{List of Abbreviations} % The \section*{} command stops section numbering

\addcontentsline{toc}{section}{List of Abbreviations} % Adds this section to the table of contents


\begin{acronym}[EuGH]
  \acro{api}[API]{Application Programming Interface}
  \acro{cc}[C\&C]{Command and Control}
  \acro{csv}[CSV]{Comma-separated Values}
  \acro{dga}[DGA]{Domain Generation Algorithm}
  \acro{dhcp}[DHCP]{Dynamic Host Configuration Protocol}
  \acro{dns}[DNS]{Domain Name System}
  \acro{dnssec}[DNSSEC]{Domain Name System Security Extensions}
  \acro{doh}[DoH]{DNS over HTTPS}
  \acro{dot}[DoT]{DNS over TLS}
  \acro{gui}[GUI]{Graphical User Interface}
  \acro{html}[HTML]{Hypertext Markup Language}
  \acro{http}[HTTP]{Hypertext Transfer Protocol}
  \acro{https}[HTTPS]{Hypertext Transfer Protocol Secure}
  \acro{iad}[IAD]{Integrated Access Device}
  \acro{ids}[IDS]{Intrusion Detection System}
  \acro{iot}[IoT]{Internet of Things}
  \acro{ip}[IP]{Internet Protocol}
  \acro{ipv4}[IPv4]{Internet Protocol Version 4}
  \acro{ipv6}[IPv6]{Internet Protocol Version 6}
  \acro{ips}[IPS]{Intrusion Prevention System}
  \acro{isp}[ISP]{Internet Service Provider}
  \acro{ml}[ML]{Machine Learning}
  \acro{mldns}[MLDNS]{Machine-Learning DNS}
  \acro{ncn}[NCN]{Nearest Centroid Neighborhood}
  \acro{protobuf}[protobuf]{Protocol Buffers}
  \acro{url}[URL]{Uniform Resource Locator}
  \acro{ram}[RAM]{Random Access Memory}
  \acro{rest}[REST]{Representational State Transfer}
  \acro{rpi4}[RPI 4]{Raspberry Pi 4 Model B}
  \acro{rr}[RR]{Resource Record}
  \acro{tcp}[TCP]{Transmission Control Protocol}
  \acro{tls}[TLS]{Transport Layer Security}
  \acro{ttl}[TTL]{Time-To-Live}

\end{acronym}

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

\phantomsection
\bibliographystyle{unsrt}
\bibliography{references.bib}

%----------------------------------------------------------------------------------------

\end{document}